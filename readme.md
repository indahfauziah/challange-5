# Challange 5 - Car Management Systems
---

# How to install
- yarn install
- yarn sequelize db:create
- yarn sequelize db:migrate
- yarn dev

## Database Structure

| Column    | Type                     |
| --------- | ------------------------ |
| id        | integer                  |
| name      | character(255)           |
| price     | float                    |
| size      | character(255)           |
| foto      | character(255)           |
| createdAt | timestamp with time zone |
| updatedAt | timestamp with time zone |

